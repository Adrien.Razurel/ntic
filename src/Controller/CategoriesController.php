<?php

namespace App\Controller;

use App\Controller;

class CategoriesController extends AppController
{
  public function index()
  {
    $this->loadComponent('Paginator');
    $categories = $this->Paginator->paginate($this->Categories->find());
    $this->set(compact('categories'));
  }

  public function add()
  {
    $category = $this->Categories->newEmptyEntity();

    if($this->request->is('post'))
    {
      $category = $this->Categories->patchEntity($category, $this->request->getData());

      if ($this->Categories->save($category))
      {
        $this->Flash->success('Nouvelle catégorie créée');
        return $this->redirect(['controller' => 'Projects','action' => 'index']);
      }
      $this->Flash->error('La catégorie n\'a pas pu être créée');
    }

    $this->set(compact('category'));
  }

  public function delete()
  {
    $this->request->allowMethod(['get','post', 'delete']);

    $this->loadComponent('Paginator');
    $categories = $this->Paginator->paginate($this->Categories->find());
    $this->set(compact('categories'));

    $category = $this->Categories->newEmptyEntity();


    if($this->request->is('post'))
    {
      $category = $this->Categories->patchEntity($category, $this->request->getData());
      $c = $this->Categories->findById($category['category_id'])->firstOrFail();

      if ($this->Categories->delete($c))
      {
        $this->Flash->success('Catégorie supprimée');
        return $this->redirect(['controller' => 'Projects','action' => 'index']);
      }
      $this->Flash->error('La catégorie n\'a pas pu être supprimée');
    }

    $this->set(compact('category'));
  }

}
