<?php

namespace App\Controller;

use App\Controller;
use Cake\Event\EventInterface;


class ProjectsController extends AppController
{

  public function beforeFilter(EventInterface $event)
  {
    parent::beforeFilter($event);

    $this->Authentication->addUnauthenticatedActions(['index', 'view', 'currentYear', 'byCategories', 'currentProjects']);
  }

  public function index()
  {
    $this->loadComponent('Paginator');
    $projects = $this->Paginator->paginate($this->Projects->find('all', ['order' => 'year DESC', 'conditions' => ['state' => True]]));
    $this->set(compact('projects'));


    $this->loadModel('Categories');
    $categories = $this->Paginator->paginate($this->Categories->find());
    $this->set(compact('categories'));

  }

  public function byCategories(){
    $this->loadComponent('Paginator');
    $this->loadModel('Categories');

    $projects = $this->Paginator->paginate($this->Projects->find('all', ['order' => ['category_id'], 'conditions' => ['state' => True]]));
    $this->set(compact('projects'));

    $categories = $this->Paginator->paginate($this->Categories->find('all', ['order' => ['title']]));
    $this->set(compact('categories'));
  }

  public function currentYear()
  {
    $this->loadComponent('Paginator');
    $projects = $this->Paginator->paginate($this->Projects->find('all', ['conditions' => ['year >=' => date("Y")-1, 'state' => True]]));
    $this->set(compact('projects'));

    $this->loadModel('Categories');
    $categories = $this->Paginator->paginate($this->Categories->find());
    $this->set(compact('categories'));
  }

  public function view($slug = null)
  {
    $this->loadComponent('Paginator');

    $this->loadModel('Categories');
    $categories = $this->Paginator->paginate($this->Categories->find());
    $this->set(compact('categories'));

    $project = $this->Projects->findBySlug($slug)->firstOrFail();
    $this->set(compact('project'));
  }

  public function currentProjects()
  {
    $this->loadComponent('Paginator');
    $projects = $this->Paginator->paginate($this->Projects->find('all', ['conditions' => ['state' => False]]));
    $this->set(compact('projects'));

    $this->loadModel('Categories');
    $categories = $this->Paginator->paginate($this->Categories->find());
    $this->set(compact('categories'));
  }

  public function add()
  {
    $this->loadComponent('Paginator');
    $this->loadModel('Categories');
    $categories = $this->Paginator->paginate($this->Categories->find());
    $this->set(compact('categories'));

    $project = $this->Projects->newEmptyEntity();

    if($this->request->is('post'))
    {
      $project = $this->Projects->patchEntity($project, $this->request->getData());


      if (!$project->getErrors())
      {
        $image = $this->request->getData('image_file');
        $image_name = $image->getClientFilename();
        $targetPath = WWW_ROOT.'img'.DS.$image_name;
        if($image_name)
        {
          $image->moveTo($targetPath);
          $project->image = $image_name;
        }
      }

      if (!$project->getErrors())
      {
        $rapport = $this->request->getData('rapport_file');
        $rapport_name = $rapport->getClientFilename();
        $targetPath2 = WWW_ROOT.'rapport'.DS.$rapport_name;
        if($rapport_name)
        {
          $rapport->moveTo($targetPath2);
          $project->rapport = $rapport_name;
        }
      }

      if (!$project->getErrors())
      {
        $meeting = $this->request->getData('meeting_file');
        $meeting_name = $meeting->getClientFilename();
        $targetPath3 = WWW_ROOT.'meeting'.DS.$meeting_name;
        if($meeting_name)
        {
          $meeting->moveTo($targetPath3);
          $project->meeting = $meeting_name;
        }
      }


      if ($this->Projects->save($project))
      {
        $this->Flash->success('Le project a été sauvegardé');
        return $this->redirect(['action' => 'index']);
      }
      $this->Flash->error('Le projet n a pas pu être sauvergardé');
    }

    $this->set(compact('project'));
  }



  public function edit($slug)
  {
    $this->loadComponent('Paginator');
    $this->loadModel('Categories');
    $categories = $this->Paginator->paginate($this->Categories->find());
    $this->set(compact('categories'));

    $project = $this->Projects->findBySlug($slug)->firstOrFail();
    $img_tmp = $project->image;
    $targetPath_img = WWW_ROOT.'img'.DS.$img_tmp;
    $rapport_tmp = $project->rapport;
    $targetPath_rapport = WWW_ROOT.'rapport'.DS.$rapport_tmp;
    $meeting_tmp = $project->meeting;
    $targetPath_meeting = WWW_ROOT.'meeting'.DS.$meeting_tmp;

    if ($this->request->is(['post', 'put']))
    {
       $project = $this->Projects->patchEntity($project, $this->request->getData());

       if (!$project->getErrors())
       {
         $image = $this->request->getData('image_file');
         $image_name = $image->getClientFilename();
         $targetPath = WWW_ROOT.'img'.DS.$image_name;
         if($image_name)
         {
           if ($img_tmp != 'no-image.svg')
           {
             unlink($targetPath_img);
           }
           $image->moveTo($targetPath);
           $project->image = $image_name;
         }
       }

       if (!$project->getErrors())
       {
         $rapport = $this->request->getData('rapport_file');
         $rapport_name = $rapport->getClientFilename();
         $targetPath2 = WWW_ROOT.'rapport'.DS.$rapport_name;
         if($rapport_name)
         {
           $rapport->moveTo($targetPath2);
           $project->rapport = $rapport_name;
         }
       }

       if (!$project->getErrors())
       {
         $meeting = $this->request->getData('meeting_file');
         $meeting_name = $meeting->getClientFilename();
         $targetPath3 = WWW_ROOT.'meeting'.DS.$meeting_name;
         if($meeting_name)
         {
           $meeting->moveTo($targetPath3);
           $project->meeting = $meeting_name;
         }
       }

       if ($this->Projects->save($project))
       {
         $this->Flash->success('Le project a été modifié');
         return $this->redirect(['action' => 'index']);
       }
       $this->Flash->error('Le projet n a pas pu être modifié');
    }

    $this->set(compact('project'));
  }

  public function delete($slug)
  {
    $this->request->allowMethod(['get','post', 'delete']);

    $project = $this->Projects->findBySlug($slug)->firstOrFail();
    $img_tmp = $project->image;
    $targetPath = WWW_ROOT.'img'.DS.$img_tmp;
    if ($this->Projects->delete($project))
    {
      if ($img_tmp != 'no-image.svg')
      {
        unlink($targetPath);
      }
      $this->Flash->success(__('Le projet a été supprimé', $targetPath));
      return $this->redirect(['action' => 'index']);
    }
  }




}
