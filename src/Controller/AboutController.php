<?php

namespace App\Controller;

use App\Controller;
use Cake\Event\EventInterface;

class AboutController extends AppController
{
  public function beforeFilter(EventInterface $event)
  {
    parent::beforeFilter($event);

    $this->Authentication->addUnauthenticatedActions(['index']);
  }

  public function index()
  {

  }

}
