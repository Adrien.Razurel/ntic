<h1>Login</h1>
<div class="users form">
  <?= $this->Form->create() ?>
  <fieldset>
    <legend>Veuillez entrer le nom d'utilisateur et le mot de passe</legend>
    <?= $this->Form->control('username',['required'=>true, 'autocomplete' => 'off'])?>
    <?= $this->Form->control('password',['required'=>true])?>
  </fieldset>
  <?= $this->Form->submit(_('Login'))?>
  <?= $this->Form->end() ?>

  <!-- <?= $this->Html->link("Ajouter un utilisateur", ['action'=> 'add'])?> -->

</div>
