<?php $cat_dico = array();
foreach ($categories as $c)
{
  $cat_dico[$c->id]= $c->title;
};
?>

<?php $sate_dico = array(False => "En cours", True => "Terminé") ?>

<h1><?= h($project->title) ?></h1>


<div class="image-project">
  <img src="<?= @$this->Url->image($project->image) ?>">
</div>
<div class="project-content">
<p><b>Description : </b><?= $project->description ?></p>
<p><b>Catégorie : </b><?= $cat_dico[$project->category_id] ?></p>
<p><b>Proposé par</b> : <?= $project->client ?></p>
<p><b>Réalisé par</b> : <?= $project->author ?></p>
<p><b>Année</b> : <?= $project->year ?></p>
<p><b>Etat</b> : <?= $sate_dico[$project->state] ?></p>
<p><b>Ressources</b> :
  <?php if ($project->rapport): ?>
   &nbsp;<a target="_blank" rel="noopener noreferrer" href=" <?= WWW_ROOT.'rapport'.DS.$project->rapport; ?>" > Rapport</a>
  <?php endif ?>
  <?php if ($project->site): ?>
   &nbsp;<a target="_blank" rel="noopener noreferrer" href="<?= $project->site ?>">Site internet</a>
  <?php endif ?>
  <?php if ($project->gitlab): ?>
   &nbsp;<a target="_blank" rel="noopener noreferrer" href="<?= $project->gitlab ?>">Gitlab</a>
  <?php endif ?>
  </p>
<br>
</div>

<br>
<br>
<?php if ($authUser): ?>
  <button onclick="window.location.href='<?= $this->Url->build(['controller' => 'Projects','action' => 'edit', $project->slug]) ?>'" id="editBtn" title="Modifier">Modifier</button>
  <button onclick="window.location.href='<?= $this->Url->build(['controller' => 'Projects','action' => 'delete', $project->slug]) ?>'" id="supprBtn" title="Supprimer">Supprimer</button>
<?php endif ?>
