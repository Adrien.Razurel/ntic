<h1>Ajouter un projet</h1>

<?php $cat_dico = array();
foreach ($categories as $c)
{
  //$array = array($c->id => $c->title);
  $cat_dico[$c->id]= $c->title;
};
?>

<?php $sate_dico = array(False => "En cours", True => "Terminé") ?>

<?php
  echo $this->Form->create($project, ['type' => 'file']);
  echo $this->Form->control('user_id', ['type' => 'hidden', 'value' => 1]);
  echo $this->Form->control('title', array('required' => true, 'maxlength' => 250, 'autocomplete' => 'off'));
  echo $this->Form->label('Category');
  echo $this->Form->select('category_id', $cat_dico);
  echo $this->Form->control('year', array('required' => true, 'min' => '2000', 'max' => date("Y")));
  echo $this->Form->control('author', array('maxlength' => 250, 'autocomplete' => 'off'));
  echo $this->Form->control('client', array('maxlength' => 250, 'autocomplete' => 'off'));
  echo $this->Form->control('description',array('autocomplete' => 'off'));
  echo $this->Form->control('site', array('maxlength' => 250, 'autocomplete' => 'off'));
  echo $this->Form->control('gitlab', array('maxlength' => 250, 'autocomplete' => 'off'));
  echo $this->Form->label('State');
  echo $this->Form->select('state', $sate_dico);
  echo $this->Form->control('image_file', array('type' => 'file','extension' => 'png'));
  echo $this->Form->control('rapport_file', array('type' => 'file'));
  echo $this->Form->control('meeting_file', array('type' => 'file'));
  echo $this->Form->Button('Save');
  echo $this->Form->end();
?>
