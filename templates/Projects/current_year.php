<?php $cat_dico = array();
foreach ($categories as $c)
{
  $cat_dico[$c->id]= $c->title;
};
?>

<h1>Années <?= date("Y")-1 ?> et <?= date("Y") ?></h1>
<article class="all-projects">
  <?php $y = 0 ?>
  <?php foreach ($projects as $p): ?>
    <?php if ($p->year = date("Y")): ?>
      <article class="project" >
        <div class="image-wrapper" onclick="location.href='<?= $this->Url->build(['action' => 'view', $p->slug]) ?>';" style="cursor: pointer;">
          <?= @$this->Html->image($p->image) ?>
      </div>
      <div class="project-content">
        <h3><?= $p->title ?></h3>
        <p><b>Description : </b><?= $p->description ?></p>
        <p><b>Catégorie : </b><?= $cat_dico[$p->category_id] ?></p>
        <p><b>Proposé par</b> : <?= $p->client ?></p>
        <p><b>Réalisé par</b> : <?= $p->author ?></p>
        <p><b>Ressources</b> :
          <?php if ($p->rapport): ?>
           &nbsp;<a target="_blank" rel="noopener noreferrer" href="<?= $this->Url->build(DS.'webroot'.DS.'rapport'.DS.$p->rapport) ?>" > Rapport</a>
          <?php endif ?>
          <?php if ($p->site): ?>
           &nbsp;<a target="_blank" rel="noopener noreferrer" href="<?= $p->site ?>">Site internet</a>
          <?php endif ?>
          <?php if ($p->gitlab): ?>
           &nbsp;<a target="_blank" rel="noopener noreferrer" href="<?= $p->gitlab ?>">Gitlab</a>
          <?php endif ?>
          </p>
        <br>
      </div>
    </article>
      <?php endif ?>
  <?php endforeach ?>
</article>
