<?php $cat_dico = array();
foreach ($categories as $c)
{
  $cat_dico[$c->id]= $c->title;
};
?>

<div class="parent-toggle">
  <input id="toggle-on" class="toggle toggle-left" name="toggle" value="false" type="radio" checked><label for="toggle-on" class="toggleBtn">Par année</label><input id="toggle-off" class="toggle toggle-right" name="toggle" value="true" type="radio" onclick="window.location.href='<?= $this->Url->build(['controller' => 'Projects', 'action' => 'byCategories']) ?>'"><label for="toggle-off" class="toggleBtn">Par catégorie</label>
</div>
<br>

<article class="all-projects">
  <?php $y = 0 ?>
  <?php foreach ($projects as $p): ?>
    <?php if ($p->year != $y): ?>
      <?php $y = $p->year ?>
      <br>
      <h2><?php echo $y-1 ?>-<?php echo $y ?></h2>
    <?php endif ?>
    <article class="project" >
      <div class="image-wrapper" onclick="location.href='<?= $this->Url->build(['action' => 'view', $p->slug]) ?>';" style="cursor: pointer;">
          <?= @$this->Html->image($p->image) ?>
      </div>
      <div class="project-content">
        <h3><?= $p->title ?></h3>
        <p><b>Description : </b><?= $p->description ?></p>
        <p><b>Catégorie : </b><?= $cat_dico[$p->category_id] ?></p>
        <p><b>Proposé par</b> : <?= $p->client ?></p>
        <p><b>Réalisé par</b> : <?= $p->author ?></p>
        <p><b>Ressources</b> :
          <?php if ($p->rapport): ?>
           &nbsp;<a target="_blank" rel="noopener noreferrer" href=" <?= $this->Url->build(DS.'webroot'.DS.'rapport'.DS.$p->rapport) ?>" > Rapport</a>
          <?php endif ?>
          <?php if ($p->site): ?>
           &nbsp;<a target="_blank" rel="noopener noreferrer" href="<?= $p->site ?>">Site internet</a>
          <?php endif ?>
          <?php if ($p->gitlab): ?>
           &nbsp;<a target="_blank" rel="noopener noreferrer" href="<?= $p->gitlab ?>">Gitlab</a>
          <?php endif ?>
          </p>
        <br>
      </div>
    </article>
  <?php endforeach ?>
</article>
