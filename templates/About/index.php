<h1>À propos</h1>
<br>
<h4>Objectif : </h4>Réalisation d’un projet en nouvelles Technologies de l’Information et de la Communication.
<br>
<br>
<br>
<h4>Contenu : </h4>Le cadre général de ces projets est la mise en pratique ou l’étude de technologies du Web.
<br>
<br>
Les projets sont proposés par des membres de l'Université de Genève (enseignants, chercheurs, PAT,
associations d'étudiants) et correspondent à l'expression d'un besoin réel.
<br>
<br>
Les domaines d'application sont : le partage et la diffusion de ressources, la diffusion d'information, la
mise en valeur de travaux de recherches, la création de sites Web à orientation pédagogique, les
technologies e-learning et les CMS, les technologies mobiles, etc.
<br>
<br>
La conception est centrée sur l'utilisateur : un accent particulier est mis sur l'analyse des besoins et sur la
validation de chaque étape de développement par les utilisateurs.
