<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'Projets NTIC';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
      <?= $cakeDescription ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

    <?= $this->Html->css(['normalize.min', 'milligram.min', 'cake', 'navbar', 'buttonstyle', 'article','footer']) ?>
    <?= $this->Html->script(['topbutton']) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <div class= "top-div">
      <div class= "logo-div">
        <div class = "sub-logo-div">
          <a href="https://www.unige.ch/"><img src = "<?= $this->Url->image('unige-logo-blanc.svg') ?>" style="width:160px"></a>
        </div>
      </div>
      <nav class="top-nav">
          <div class="top-nav-title">
            <a href="<?= $this->Url->build('/') ?>"><span>PROJETS NTIC</span></a><br>
            <span>Liste des projets proposés pour le cours NTIC</span>
          </div>
          <?php if ($authUser): ?>
            <div class="top-nav-links">
                <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'logout']) ?>"><span>DECONNEXION</span></a>
            </div>
          <?php else: ?>
            <div class="top-nav-links">
                <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'login']) ?>"><span>CONNEXION</span></a>
            </div>
          <?php endif ?>
      </nav>
        <div class="nav">
        <ul class="navbar">
          <li><a class="active" href="<?= $this->Url->build(['controller' => 'Projects', 'action' => 'currentYear']) ?>">Années <?= date("Y")-1 ?> et <?= date("Y")?></a></li>
          <li><a class="active" href="<?= $this->Url->build(['controller' => 'Projects', 'action' => 'index']) ?>">Toutes les années</a></li>
          <li><a class="active" href="<?= $this->Url->build(['controller' => 'Projects', 'action' => 'currentProjects']) ?>">Projets en cours</a></li>
          <li><a class="active" href="<?= $this->Url->build(['controller' => 'About', 'action' => 'index']) ?>">À propos</a></li>
          <?php if ($authUser): ?>
            <li style="float:right"><a class="active-right">Paramètres Admin</a>
                <ul>
                  <li><a class="sub" href="<?= $this->Url->build(['controller' => 'Projects', 'action' => 'add']) ?>">Ajouter un projet</a></li>
                  <li><a class="sub" href="<?= $this->Url->build(['controller' => 'Categories', 'action' => 'add']) ?>">Ajouter une catégorie</a></li>
                  <li><a class="sub" href="<?= $this->Url->build(['controller' => 'Categories', 'action' => 'delete']) ?>">Retirer une catégorie</a></li>
                  <li><a class="sub" href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'index']) ?>">Gestion utlisateur</a></li>
                </ul>
            </li>
          <?php endif ?>
        </ul>
      </div>
    </div>
    <main class="main">
        <div class="container">
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </div>
    </main>
    <button onclick="window.scrollTo({top: 0});" id="myBtn" title="Go to top"><i class="arrow up"></i></button>
    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <h6>CONTACT</h6>
            <ul class="footer-links">
              <li><a href="mailto:Luka.Nerima@unige.ch">Envoyez-nous une question ou un commentaire </a></li>
            </ul>
          </div>
          <div class="col-xs-6 col-md-3">
            <h6>LIENS</h6>
            <ul class="footer-links">
              <li><a href="https://www.unige.ch/lettres/linguistique/welcome/">Département de linguistique</a></li>
              <li><a href="https://moodle.unige.ch/course/view.php?id=9027">NTIC Moodle</a></li>
            </ul>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text">Copyright &copy; 2021 Tous droits réservés |
         <a href="https://www.unige.ch/">Université de Genève</a>.
            </p>
          </div>
        </div>
      </div>
</footer>
</body>
</html>
