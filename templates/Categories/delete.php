<br>
<br>
<h1>Catégorie à supprimer</h1>

<?php $cat_dico = array();
foreach ($categories as $c)
{
  $cat_dico[$c->id]= $c->title;
};
?>

<?php
  echo $this->Form->create($category);
  echo $this->Form->label('Category');
  echo $this->Form->select('category_id', $cat_dico);
  echo $this->Form->Button('Delete');
  echo $this->Form->end();
?>
