<br>
<br>
<h1>Nouvelle catégorie</h1>

<?php
  echo $this->Form->create($category);
  echo $this->Form->control('category_id', ['type' => 'hidden']);
  echo $this->Form->control('title', array('required' => true, 'maxlength' => 250, 'autocomplete' => 'off'));
  echo $this->Form->Button('Save');
  echo $this->Form->end();
?>
